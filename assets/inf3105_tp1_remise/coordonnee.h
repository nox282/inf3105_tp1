/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Été 2016 / TP1                                       *
 *  http://ericbeaudry.ca/INF3105/tp1                    */

#if !defined(_COORDONNEE__H_)
#define _COORDONNEE__H_
#include <iostream>

class Coordonnee {
    friend struct Vecteur;
    public:
        double distance(const Coordonnee&) const;
        Coordonnee(double _lon = 0, double _lat = 0):latitude(_lat), longitude(_lon){};
    private:
        double latitude;
        double longitude;

    friend std::ostream& operator << (std::ostream&, const Coordonnee&);
    friend std::istream& operator >> (std::istream&, Coordonnee&);
};

typedef struct point{// Permet de faciliter les tests sur les vecteurs.
    int x;
    int y;
    point(int _x, int _y): x(_x), y(_y){};
    friend std::ostream &operator << (std::ostream &os, const point &a){
        os << '(' << a.x << ", " << a.y << ')';
        return os;
    }
}point_t; //Aucune incidence sur le fonctionnement du programme.

typedef struct Vecteur{
    double x;
    double y;
    
    Vecteur(double _x=0, double _y=0):x(_x), y(_y){};
    
    Vecteur(const Coordonnee &a, const Coordonnee &b){  // vec(ab) = (Xb -Xa, Yb - Ya)
        x = b.longitude - a.longitude;
        y = b.latitude - a.latitude;
    }

    double operator *(const Vecteur &v) const{          // Produit scalaire
        return (x*v.x)+(y*v.y);
    };

    Coordonnee operator +(const Coordonnee &c) const{   // point Cab = vec(ab) + point(c)
        return Coordonnee(c.longitude + x, c.latitude + y);
    };

    Vecteur operator *(const double r) const{           // r * vec(ab) = (Xab * r, Yab * r)
        return Vecteur(r*x, r*y); 
    }

    static Coordonnee projeter(const Coordonnee &a, const Coordonnee &b, const Coordonnee &c){
        Vecteur ab(a, b), ac(a, c); 
        double ratio = (ac * ab)/(ab * ab);
         
        if(ratio >= 1)
            return b; // Si la projection tombe plus loin que b on retourne b.
        else if(ratio <= 0)
            return a; // Si la projection tombe plus loin que a on retourne a.
        Vecteur acab = ab * ratio;
        
        return acab + a;
    }

    static point_t projeter(const point_t &a, const point_t &b, const point_t &c){
        Vecteur ab(b.x - a.x, b.y - a.y), ac(c.x- a.x, c.y - a.y);
        double ratio = (ac * ab)/(ab * ab);
        
        if(ratio >= 1)
            return b;
        else if(ratio <= 0)
            return a;
        Vecteur acab = ab * ratio;
        
        return point_t(acab.x + a.x, acab.y + a.y);
    }
}vec_t;

#endif
