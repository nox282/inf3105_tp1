/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Été 2016 / TP1                                       *
 *  http://ericbeaudry.ca/INF3105/tp1                    */

#if !defined(__CARTE_)
#define __CARTE_

#include <string>
#include "coordonnee.h"
#include "tableau.h"

class Route{
    friend class Carte;
    private:
        std::string nom;
        Tableau<Coordonnee> points;
        int debut;
        int fin;
    public:
        double getLongueur() const;
        std::string getNom() const{return nom;};
        Route():nom(""), points(Tableau<Coordonnee>()), debut(0), fin(0){};

    friend std::istream &operator >> (std::istream&, Route&);  
};

class Carte{
    private:
        Tableau<Route> routes;
        int calculerNPorte(const Route&, const double) const;
    public:
        std::string geocodageinverse(const Coordonnee&) const;
        Carte():routes(Tableau<Route>()){};

  friend std::istream& operator >> (std::istream&, Carte&);
};

#endif
