/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Été 2016 / TP1                                       *
 *  http://ericbeaudry.ca/INF3105/tp1                    */

#if !defined(__CARTE_)
#define __CARTE_

#include <string>
#include "../structure_donnee/coordonnee.h"
#include "../structure_donnee/tableau.h"

class Route{
    friend class Carte;
    private:
        std::string nom;
        Tableau<Coordonnee> points;
        int debut;
        int fin;
    public:
        double getLongueur() const;
        std::string getNom() const{return nom;};

    friend std::istream &operator >> (std::istream&, Route&);  
};

class Carte{
    public:
        std::string geocodageinverse(const Coordonnee&) const;
  
    private:
        Tableau<Route> routes;
        int calculerNPorte(const Route&, const double) const;
        

  friend std::istream& operator >> (std::istream&, Carte&);

};

#endif
