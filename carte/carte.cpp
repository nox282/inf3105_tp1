/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Été 2016 / TP1                                       *
 *  http://ericbeaudry.ca/INF3105/tp1                    */

#include "carte.h"
#include <cmath>
#include <istream>
#include <climits>
#include <string> 
#include <sstream>
#include <algorithm>

double Route::getLongueur() const{
    double ret = 0;
    for(int i = 1; i < points.taille(); i++)
        ret += points[i-1].distance(points[i]); // Additionne chaque segment de route.
    return ret; // Renvoie la longueur totale.
}

std::string Carte::geocodageinverse(const Coordonnee& c) const{
    double distance = 0, dmin = INT_MAX;
    int porte;
    Coordonnee a, b, cP;
    std::stringstream sstr;

    for(int i = 0; i < routes.taille(); i++){ // Pour chaque route de la carte
        distance = 0;
        for(int j = 1; j < routes[i].points.taille(); j++){    // Pour chaque segments d'une route
            a = routes[i].points[j-1];                         // a = debut segment
            b = routes[i].points[j];                           // b = fin segment
            cP = vec_t::projeter(a, b, c);                          // c = point requete, cP = point projete
        
            if(c.distance(cP) < dmin){
                dmin = c.distance(cP);
                porte = calculerNPorte(routes[i], a.distance(cP)+distance);
                
                sstr.clear();
                sstr.str(std::string());
                sstr << porte << ' ' << routes[i].nom;
            }
            distance += a.distance(b);                              // distance parcouru a un index j
        }
    }
    return sstr.str();
}


int Carte::calculerNPorte(const Route &route, const double lp) const{
    double l = route.getLongueur();
    double llp = lp/l;                                              // lp = segments parcouru + a.distance(cP)
    int noPorte = round(((route.fin-route.debut)*llp)/2)*2; 
    return route.debut + noPorte;
}

std::istream &operator >> (std::istream &is, Route &route)
{
    char temp;
    std::string chaine;
    
    // Lecture du nom de rue
    is >> chaine;
    if(!is || chaine.empty()) // détecter une anomalie
        return is;
    route.nom = chaine;

    is >> temp;
    assert(temp==':');
    
    // Lecture de l'intervalle des numéros de porte
    is >> route.debut >> temp >> route.fin;
    assert(temp=='-');
    
    is >> temp;
    assert(temp==':');
    
    // Lecture des coordonnées
    do{    
        Coordonnee c;
        is >> c >> temp;
        route.points.ajouter(c);
    }while(temp==',');
    assert(temp==';');
    return is;
}

std::istream& operator >> (std::istream &is, Carte &carte)
{
    while(is){
        Route route;
        is >> route >> std::ws;
        if(!route.getNom().empty())                                 // Ne pas ajouter d'element vide 
            carte.routes.ajouter(route);
    }
    return is;
}
