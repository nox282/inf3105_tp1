include Makefile.inc

EXEC=tp1

CARTEDIR=carte
DATADIR=structure_donnee

SRC=$(wildcard *.cpp)
OBJ=$(SRC:.cpp=.o)

SUBOBJ=$(wildcard $(CARTEDIR)/*.o)\
	   $(wildcard $(DATADIR)/*.o)\
	   $(wildcard $(DATADIR)/*.gh)

all : tp1

tp1 : tp1.cpp carte data
	$(CXX) $(OPTIONS) tp1.cpp $(SUBOBJ) -o $(EXEC)

.PHONY:$(CARTEDIR)

carte : 
	$(MAKE) -C $(CARTEDIR)

.PHONY:$(DATADIR)

data : 
	$(MAKE) -C $(DATADIR)
	
clean:
	rm -rf tp1 *~ *.o
	rm -rf $(CARTEDIR)/*.o $(CARTEDIR)/*.gh
	rm -rf $(DATADIR)/*.o $(DATADIR)/*.gh
	rm -rf *.txt
