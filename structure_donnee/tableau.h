/* UQAM / Département d'informatique
   INF3105 - Structures de données et algorithmes
   Squelette pour classe générique Tableau<T> pour le TP1 et Lab3.
   AUTEUR(S): Nicolas Hamard HAMN25079209 
*/

#if !defined(__CLASSE_TABLEAU_H__)
#define __CLASSE_TABLEAU_H__

//DECLARATION

template <class T>
class Tableau {
  public:
    Tableau(int capacite_initiale=8);
    Tableau(const Tableau&);
    ~Tableau();

    // Ajouter un element à la fin
    void ajouter(const T& element);
    // Vider le tableau
    void vider();
    // Retourne le nombre d'éléments dans le tableau
    int taille() const;

    // Insère element à position index. Les éléments à partir de index sont décalés d'une position au préalable.
    void inserer(const T& element, int index=0);

    // Enlève l'element à position index. Les éléments après index sont décalés d'une position après.
    void enlever(int index=0);

    // Cherche et retourne la position de l'élément. Si non trouvé, retourne -1.
    // Voir la méthode "trouver" dans les notes de cours. Il suffit de l'adapter pour retourner la position au lieu d'un booléen
    int trouver(const T& element);
    T& operator[] (int index);
    const T& operator[] (int index) const;
    Tableau<T>& operator = (const Tableau<T>& autre);
    bool operator == (const Tableau<T>& autre) const;

  private:
    T* elements;
    int capacite;
    int nbElements;
    void redimensionner();
    void echanger(Tableau<T> &autre);
};

//DECLARATION
//----------------------------------------------------------------------------
//IMPLEMENTATION

#include <assert.h>
#include <iostream>

template <class T>
Tableau<T>::Tableau(int capacite_initiale):elements(new T[capacite_initiale]), capacite(capacite_initiale), nbElements(0){}

template <class T>
Tableau<T>::Tableau(const Tableau& autre):elements(new T[autre.taille()]), capacite(autre.taille()) {
    for(int i = 0; i < autre.nbElements; i++)
        ajouter(autre.elements[i]);
}

template <class T>
Tableau<T>::~Tableau() {
    delete[] elements;
}

template <class T>
int Tableau<T>::taille() const {
    return nbElements;
}

template <class T>
void Tableau<T>::ajouter(const T& item) {
    redimensionner();
    elements[nbElements++] = item;
}

template <class T>
void Tableau<T>::inserer(const T& item, int index) {
    assert(index >= 0);

    Tableau<T> temp(*this);
    vider();
    for(int i = 0; i < temp.taille(); i++){
        if(i == index){
            ajouter(item);
            ajouter(temp[i]);
        }
        else
            ajouter(temp[i]);
    }
}

template <class T>
void Tableau<T>::enlever(int index) {
    assert(index >= 0 && index <= taille());
    
    Tableau<T> temp(*this);
    vider();
    for(int i = 0; i < temp.taille(); i++){
        if(i != index)
            ajouter(temp[i]);
    }
}

template <class T>
int Tableau<T>::trouver(const T& item) {
    for(int i = 0; i < taille(); i++)
        if(elements[i] == item)
            return i;
    return -1;
}

template <class T>
void Tableau<T>::vider() {
    delete[] elements;
    nbElements = 0;
    capacite = 8;
    elements = new T[8];
}

template <class T>
T& Tableau<T>::operator[] (int index) {
    assert(index >= 0 && index < taille());
    return elements[index];
}

template <class T>
const T& Tableau<T>::operator[] (int index) const {
    assert(index >= 0 && index < taille());
    return elements[index];
}

template <class T>
Tableau<T>& Tableau<T>::operator = (const Tableau<T>& autre){ 
    Tableau<T>(autre).echanger(*this);
    return *this;
}

template <class T>
bool Tableau<T>::operator == (const Tableau<T>& autre) const {
    if(taille() != autre.taille())
        return false;
    else{
        for(int i = 0; i < taille(); i++)
            if(elements[i] != autre.elements[i])
                return false;
    }
    return true;
}

template <class T>
void Tableau<T>::redimensionner(){
    if(taille() >= capacite){
        int tempNbElements = taille();
        int tempCapacite = capacite*2;
        T* tab = new T[tempCapacite];
        
        for(int i = 0; i < taille(); i++)
            tab[i] = elements[i];
        
        delete[] elements;

        elements = tab;
        nbElements = tempNbElements;
        capacite = tempCapacite;
    }
}

template <class T>
void Tableau<T>::echanger(Tableau<T> &autre){
    int tempCapacite, tempNbElements;
    T *tempElements;
    
    tempCapacite = autre.capacite;
    autre.capacite = capacite;
    capacite = tempCapacite;

    tempNbElements = autre.nbElements;
    autre.nbElements = nbElements;
    nbElements = tempNbElements;

    tempElements = autre.elements;
    autre.elements = elements;
    elements = tempElements;
}

//IMPLEMENTATION
#endif
